const graphql = require('graphql');
var _ = require('lodash');

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInt,
    GraphQLSchema,
    GraphQLList,
    GraphQLBoolean,
} = graphql

const Person = new GraphQLObjectType({
    name: 'Person',
    description: 'Represent a person entity',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: new graphql.GraphQLNonNull(GraphQLString) },
        age: { type: GraphQLInt },
        isMarried: { type: GraphQLBoolean },
        gpa: { type: graphql.GraphQLFloat },

        justAType: {
            type: Person,
            resolve(parent, args){
                return parent;
            }
        }
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    description: 'This is root query type',
    fields: {
        person: {
            type: Person,
            resolve(parent, args){
                let personObj = {
                    name: null,
                    age: 36,
                    isMarried: true,
                    gpa: 4.0
                }
                return personObj
            }

        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery

})