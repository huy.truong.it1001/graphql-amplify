const graphql = require('graphql');
var _ = require('lodash');
const User = require('../model/user');
const Hobby = require('../model/hobby');
const Post = require('../model/post');

var usersData = [
    { id: '1', name: 'Bond', age: 35, profession: 'spy' },
    { id: '2', name: 'Kenedy', age: 60, profession: 'president' },
    { id: '3', name: 'Trump', age: 72, profession: 'business man' },
    { id: '4', name: 'Biden', age: 78, profession: 'president' },
    { id: '5', name: 'Osama', age: 64, profession: 'billionare' }

];

var hobbiesData = [
    { id: '1001', title: 'Reading Books', description: 'good', userId: "1" },
    { id: '1002', title: 'Playing Games', description: 'not good', userId: "3" },
    { id: '1003', title: 'Hang out', description: 'good', userId: "4" },
    { id: '1004', title: 'Running', description: 'good', userId: "5" },
    { id: '1005', title: 'Running', description: 'good', userId: "1" }
];

var postsData = [
    { id: '2001', comment: 'Spring Boot, amazing !!!', userId: '1' },
    { id: '2002', comment: 'Fanstatic', userId: '2' },
    { id: '2003', comment: 'I love this', userId: '1' },
    { id: '2004', comment: 'How to change the world', userId: '5' },
    { id: '2005', comment: 'That is awaresome', userId: '4' }
];



const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInt,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull,
} = graphql

const UserType = new GraphQLObjectType({
    name: 'User',
    description: 'Documentation for users....',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        age: { type: GraphQLInt },
        profession: { type: GraphQLString },

        posts: {
            type: new GraphQLList(PostType),
            resolve(parent, args) {
                return Post.find({ userId: parent.id });
            }
        },
        hobbies: {
            type: new GraphQLList(HobbyType),
            resolve(parent, args) {
                return Hobby.find({ userId: parent.id });
            }
        }

    })
});

const HobbyType = new GraphQLObjectType({
    name: 'Hobby',
    description: 'Hobby descriptions',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        user: {
            type: UserType,
            resolve(parent, args) {
                return User.findById(parent.userId);
            }
        }

    })

})

const PostType = new GraphQLObjectType({
    name: 'Post',
    description: 'Post descriptions',
    fields: () => ({
        id: { type: GraphQLID },
        comment: { type: GraphQLString },
        user: {
            type: UserType,
            resolve(parent, args) {
                return User.findById(parent.userId);
            }
        }
    })

})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    description: 'This is root query type',
    fields: {
        user: {
            type: UserType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return User.findById(args.id);
            }
        },
        users: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find();
            }

        },


        hobby: {
            type: HobbyType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return Hobby.findById(args.id);
            }
        },
        hobbies: {
            type: GraphQLList(HobbyType),
            resolve(parent, args) {
                return Hobby.find();
            }

        },


        post: {
            type: PostType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return Post.findById(args.id);
            }
        },
        posts: {
            type: GraphQLList(PostType),
            resolve(parent, args) {
                return Post.find();
            }

        },
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        createUser: {
            type: UserType,
            args: {

                name: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLInt) },
                profession: { type: GraphQLString }
            },
            resolve(parent, args) {
                let user = new User({
                    name: args.name,
                    age: args.age,
                    profession: args.profession
                });
                return user.save();

            }
        },
        updateUser: {
            type: UserType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                name: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLInt) },
                profession: { type: GraphQLString }
            },
            resolve(parent, args) {
                return updateUser = User.findByIdAndUpdate(args.id,
                    {
                        $set: {
                            name: args.name,
                            age: args.age,
                            profession: args.profession
                        }
                    },
                    { new: true }    //send back the updated objectType
                )
            }
        },
        removeUser: {
            type: UserType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve(parent, args) {
                let deletedUser = User.findByIdAndDelete(args.id).exec();
                if(!deletedUser){
                    throw new("Error");
                }
                return deletedUser;
            }
        },

        createPost: {
            type: PostType,
            args: {
                //id:{type: GraphQLID},
                comment: { type: GraphQLString },
                userId: { type: GraphQLID }
            },
            resolve(parent, args) {
                let post = new Post({
                    comment: args.comment,
                    userId: args.userId
                });
                return post.save();

            }
        },
        updatePost: {
            type: PostType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                comment: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updatePost = Post.findByIdAndUpdate(args.id,
                    {
                        $set: {
                            comment: args.comment,
                        }
                    },
                    { new: true }    //send back the updated objectType
                )
            }
        },
        removePost: {
            type: PostType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve(parent, args) {
                let deletedPost = Post.findByIdAndDelete(args.id).exec();
                if(!deletedPost){
                    throw new("Error");
                }
                return deletedPost;
            }
        },

        createHobby: {
            type: HobbyType,
            args: {
                //id:{type: GraphQLID},
                title: { type: GraphQLString },
                description: { type: GraphQLString },
                userId: { type: GraphQLID }
            },
            resolve(parent, args) {
                let hobby = new Hobby({
                    title: args.title,
                    description: args.description,
                    userId: args.userId
                });
                return hobby.save();

            }
        },
        updateHobby: {
            type: HobbyType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                title: { type: GraphQLString },
                description: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updateHobby = Hobby.findByIdAndUpdate(args.id,
                    {
                        $set: {
                            title: args.title,
                            description: args.description,
                        }
                    },
                    { new: true }    //send back the updated objectType
                )
            }
        },
        removeHobby: {
            type: HobbyType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve(parent, args) {
                let deletedHobby = Hobby.findByIdAndDelete(args.id).exec();
                if(!deletedHobby){
                    throw new("Error");
                }
                return deletedHobby;
            }
        },
    }

})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation

})