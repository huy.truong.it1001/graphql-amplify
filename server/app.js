const express = require('express');
const graphqlHTTP = require('express-graphql').graphqlHTTP;
const mongoose = require('mongoose');
const schema = require('./schema/schema');
const testSchema = require('./schema/types_schema');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 4000

mongoose.connect('mongodb+srv://mongo_dev:Postgres%40%40lk1988@cluster0.brmm1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
mongoose.connection.once('open', () => {
    console.log('We are connected to Mongo Atlas');

})

app.use(cors());

app.use('/graphql', graphqlHTTP({

    graphiql: true,
    schema: schema
}))


app.listen(port, () => {
    console.log('Listening for requests');
})